import { expect } from 'chai'
import functies from '@/checkCard.js'
import createShuffle from '@/shuffledeck.js'


describe('test the card check module', () => {
  it('klaveren twee is shown as true if number is 2 and kind is 1', () => {
    let bool = functies.isKlaverenTwee(2,1)
   
    expect(bool).to.equal(true);
  
  })
  it('klaveren twee is shown as false if number is 3 and kind is 1', () => {
    let bool = functies.isKlaverenTwee(3,1)
   
    expect(bool).to.equal(false);
  
  })
  it('klaveren drie is shown as true if number is 3 and kind is 1', () => {
    let bool = functies.isKlaverenDrie(3,1)
   
    expect(bool).to.equal(true);
  
  })
  it('klaveren drie is shown as false if number is 2 and kind is 1', () => {
    let bool = functies.isKlaverenDrie(2,1)
   
    expect(bool).to.equal(false);
  
  })
})


describe('test the card shuffle function', () => {
  it('create a shuffled list of cards', () => {
    let shuffledDeck = createShuffle();
   
    console.log(shuffledDeck)

    expect(shuffledDeck.length).to.equal(4);
  
  })

})


