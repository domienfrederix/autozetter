import Vue from 'vue'
import Vuex from 'vuex'
import createShuffle from './shuffledeck'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    playerOne: "player A",
    playerTwo: "player 2",
    playerThree: "player 3",
    playerFour: "player 4",
    cardsArray: [{
      number: 7,
      kind: 1,
      inPlayerHand: true,
      onTable: false,
      played: false,
      givenToPlayer: 1
    }, {
      number: 7,
      kind: 2,
      inPlayerHand: true,
      onTable: false,
      played: false,
      givenToPlayer: 1
    },
    {
      number: 7,
      kind: 3,
      inPlayerHand: true,
      onTable: false,
      played: false,
      givenToPlayer: +1
    },
    {
      number: 7,
      kind: 4,
      inPlayerHand: true,
      onTable: false,
      played: false,
      givenToPlayer: 1
    },
    {
      number: 8,
      kind: 1,
      inPlayerHand: true,
      onTable: false,
      played: false,
      givenToPlayer: 1
    }, {
      number: 8,
      kind: 2,
      inPlayerHand: true,
      onTable: false,
      played: false,
      givenToPlayer: 1
    },
    {
      number: 8,
      kind: 3,
      inPlayerHand: true,
      onTable: false,
      played: false,
      givenToPlayer: 1
    },
    {
      number: 8,
      kind: 4,
      inPlayerHand: true,
      onTable: false,
      played: false,
      givenToPlayer: 1
    },
    {
      number: 9,
      kind: 1,
      inPlayerHand: true,
      onTable: false,
      played: false,
      givenToPlayer: 1
    }, {
      number: 9,
      kind: 2,
      inPlayerHand: true,
      onTable: false,
      played: false,
      givenToPlayer: 1
    },
    {
      number: 9,
      kind: 3,
      inPlayerHand: true,
      onTable: false,
      played: false,
      givenToPlayer: 1
    },
    {
      number: 9,
      kind: 4,
      inPlayerHand: true,
      onTable: false,
      played: false,
      givenToPlayer: 1
    },
    {
      number: 10,
      kind: 1,
      inPlayerHand: true,
      onTable: false,
      played: false,
      givenToPlayer: 1
    }, {
      number: 10,
      kind: 2,
      inPlayerHand: true,
      onTable: false,
      played: false,
      givenToPlayer: 1
    },
    {
      number: 10,
      kind: 3,
      inPlayerHand: true,
      onTable: false,
      played: false,
      givenToPlayer: 1
    },
    {
      number: 10,
      kind: 4,
      inPlayerHand: true,
      onTable: false,
      played: false,
      givenToPlayer: 1
    },
    {
      number: 11,
      kind: 1,
      inPlayerHand: true,
      onTable: false,
      played: false,
      givenToPlayer: 1
    }, {
      number: 11,
      kind: 2,
      inPlayerHand: true,
      onTable: false,
      played: false,
      givenToPlayer: 1
    },
    {
      number: 11,
      kind: 3,
      inPlayerHand: true,
      onTable: false,
      played: false,
      givenToPlayer: 1
    },
    {
      number: 11,
      kind: 4,
      inPlayerHand: true,
      onTable: false,
      played: false,
      givenToPlayer: 1
    },
    {
      number: 12,
      kind: 1,
      inPlayerHand: true,
      onTable: false,
      played: false,
      givenToPlayer: 1
    }, {
      number: 12,
      kind: 2,
      inPlayerHand: true,
      onTable: false,
      played: false,
      givenToPlayer: 1
    },
    {
      number: 12,
      kind: 3,
      inPlayerHand: true,
      onTable: false,
      played: false,
      givenToPlayer: 1
    },
    {
      number: 12,
      kind: 4,
      inPlayerHand: true,
      onTable: false,
      played: false,
      givenToPlayer: 1
    },
    {
      number: 13,
      kind: 1,
      inPlayerHand: true,
      onTable: false,
      played: false,
      givenToPlayer: 1
    }, {
      number: 13,
      kind: 2,
      inPlayerHand: true,
      onTable: false,
      played: false,
      givenToPlayer: 1
    },
    {
      number: 13,
      kind: 3,
      inPlayerHand: true,
      onTable: false,
      played: false,
      givenToPlayer: 1
    },
    {
      number: 13,
      kind: 4,
      inPlayerHand: true,
      onTable: false,
      played: false,
      givenToPlayer: 1
    },
    {
      number: 14,
      kind: 1,
      inPlayerHand: true,
      onTable: false,
      played: false,
      givenToPlayer: 1
    }, {
      number: 14,
      kind: 2,
      inPlayerHand: true,
      onTable: false,
      played: false,
      givenToPlayer: 1
    },
    {
      number: 14,
      kind: 3,
      inPlayerHand: true,
      onTable: false,
      played: false,
      givenToPlayer: 1
    },
    {
      number: 14,
      kind: 4,
      inPlayerHand: true,
      onTable: false,
      played: false,
      givenToPlayer: 1
    }],
  },
  mutations: {
    shuffle (state) {
      // mutate state
      state.cardsArray = createShuffle();

      console.log("state change");
      console.log(state.cardsArray);
      
    }
  },
  actions: {
      
  }
})
