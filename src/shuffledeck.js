
export default function createShuffle() {

  var listOfCards = [{
    number: 7,
    kind: 1,
    inPlayerHand: true,
    onTable: false,
    played: false,
    givenToPlayer: 1
  }, {
    number: 7,
    kind: 2,
    inPlayerHand: true,
    onTable: false,
    played: false,
    givenToPlayer: 1
  },
  {
    number: 7,
    kind: 3,
    inPlayerHand: true,
    onTable: false,
    played: false,
    givenToPlayer: +1
  },
  {
    number: 7,
    kind: 4,
    inPlayerHand: true,
    onTable: false,
    played: false,
    givenToPlayer: 1
  },
  {
    number: 8,
    kind: 1,
    inPlayerHand: true,
    onTable: false,
    played: false,
    givenToPlayer: 1
  }, {
    number: 8,
    kind: 2,
    inPlayerHand: true,
    onTable: false,
    played: false,
    givenToPlayer: 1
  },
  {
    number: 8,
    kind: 3,
    inPlayerHand: true,
    onTable: false,
    played: false,
    givenToPlayer: 1
  },
  {
    number: 8,
    kind: 4,
    inPlayerHand: true,
    onTable: false,
    played: false,
    givenToPlayer: 1
  },
  {
    number: 9,
    kind: 1,
    inPlayerHand: true,
    onTable: false,
    played: false,
    givenToPlayer: 1
  }, {
    number: 9,
    kind: 2,
    inPlayerHand: true,
    onTable: false,
    played: false,
    givenToPlayer: 1
  },
  {
    number: 9,
    kind: 3,
    inPlayerHand: true,
    onTable: false,
    played: false,
    givenToPlayer: 1
  },
  {
    number: 9,
    kind: 4,
    inPlayerHand: true,
    onTable: false,
    played: false,
    givenToPlayer: 1
  },
  {
    number: 10,
    kind: 1,
    inPlayerHand: true,
    onTable: false,
    played: false,
    givenToPlayer: 1
  }, {
    number: 10,
    kind: 2,
    inPlayerHand: true,
    onTable: false,
    played: false,
    givenToPlayer: 1
  },
  {
    number: 10,
    kind: 3,
    inPlayerHand: true,
    onTable: false,
    played: false,
    givenToPlayer: 1
  },
  {
    number: 10,
    kind: 4,
    inPlayerHand: true,
    onTable: false,
    played: false,
    givenToPlayer: 1
  },
  {
    number: 11,
    kind: 1,
    inPlayerHand: true,
    onTable: false,
    played: false,
    givenToPlayer: 1
  }, {
    number: 11,
    kind: 2,
    inPlayerHand: true,
    onTable: false,
    played: false,
    givenToPlayer: 1
  },
  {
    number: 11,
    kind: 3,
    inPlayerHand: true,
    onTable: false,
    played: false,
    givenToPlayer: 1
  },
  {
    number: 11,
    kind: 4,
    inPlayerHand: true,
    onTable: false,
    played: false,
    givenToPlayer: 1
  },
  {
    number: 12,
    kind: 1,
    inPlayerHand: true,
    onTable: false,
    played: false,
    givenToPlayer: 1
  }, {
    number: 12,
    kind: 2,
    inPlayerHand: true,
    onTable: false,
    played: false,
    givenToPlayer: 1
  },
  {
    number: 12,
    kind: 3,
    inPlayerHand: true,
    onTable: false,
    played: false,
    givenToPlayer: 1
  },
  {
    number: 12,
    kind: 4,
    inPlayerHand: true,
    onTable: false,
    played: false,
    givenToPlayer: 1
  },
  {
    number: 13,
    kind: 1,
    inPlayerHand: true,
    onTable: false,
    played: false,
    givenToPlayer: 1
  }, {
    number: 13,
    kind: 2,
    inPlayerHand: true,
    onTable: false,
    played: false,
    givenToPlayer: 1
  },
  {
    number: 13,
    kind: 3,
    inPlayerHand: true,
    onTable: false,
    played: false,
    givenToPlayer: 1
  },
  {
    number: 13,
    kind: 4,
    inPlayerHand: true,
    onTable: false,
    played: false,
    givenToPlayer: 1
  },
  {
    number: 14,
    kind: 1,
    inPlayerHand: true,
    onTable: false,
    played: false,
    givenToPlayer: 1
  }, {
    number: 14,
    kind: 2,
    inPlayerHand: true,
    onTable: false,
    played: false,
    givenToPlayer: 1
  },
  {
    number: 14,
    kind: 3,
    inPlayerHand: true,
    onTable: false,
    played: false,
    givenToPlayer: 1
  },
  {
    number: 14,
    kind: 4,
    inPlayerHand: true,
    onTable: false,
    played: false,
    givenToPlayer: 1
  }];


  var currentIndex = listOfCards.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = listOfCards[currentIndex];
    listOfCards[currentIndex] = listOfCards[randomIndex];
    listOfCards[randomIndex] = temporaryValue;
  }

  return listOfCards;
}